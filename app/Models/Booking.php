<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
    'uuid',
    'user_id',
    'room_id',
    'schedule_id',
    'total',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function room(){
        return $this->belongsTo(Room::class);
    }

    public function schedule(){
        return $this->belongsTo(Schedule::class);
    }
}
