<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Hotel extends Model
{
    protected $guarded = [];
    use HasFactory,SoftDeletes;

    public function rooms(){
        return $this->hasMany(Room::class);
    }

    public function path(){
        return "/hotels/{$this->slug}" ;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function image(){
        $image = $this->image ? $this->image : 'avatar.png';
        return Storage::disk('hotel')->url($image);
    }



}
