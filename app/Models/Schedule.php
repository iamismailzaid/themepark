<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class Schedule extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    protected $dates = [
        'starting',
        'due',
    ];

    public function path(){
        return "/schedules/{$this->slug}" ;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function Room(){
        return $this->belongsToMany(Room::class);
    }

    public function image(){
        $image = $this->image ? $this->image : 'avatar.png';
        return Storage::disk('schedule')->url($image);
    }

    public function booking(){
        return $this->hasMany(booking::class);
    }

}
