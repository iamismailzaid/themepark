<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use HasFactory,SoftDeletes;
    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected $guarded = [];

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

}
