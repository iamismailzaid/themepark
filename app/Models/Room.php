<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    protected $guarded = [];

    use HasFactory,SoftDeletes;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }

    public function customer(){
        return $this->belongsToMany(User::class );
    }

    public function events(){
        return $this->belongsTo(Schedule::class);
    }

    public function booking(){
        return $this->hasMany(Booking::class);
    }

    public function image(){
        $image = $this->image ? $this->image : 'avatar.png';
        return \Storage::disk('room')->url($image);
    }
}
