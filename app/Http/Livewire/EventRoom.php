<?php

namespace App\Http\Livewire;

use App\Models\Hotel;
use App\Models\Room;
use App\Models\Schedule;
use Livewire\Component;

class EventRoom extends Component
{
    public $events;
    public $hotelId;
    public $rooms;
    public $eventId;

    public function render()
    {
        $this->events = Schedule::latest();
        $this->hotels = Hotel::orderBy('name')->get();
        $this->events = Schedule::all();

         if(!empty($this->hotels)) {
             //get the rooms where given event is not booked
             $this->rooms = Room::where('hotel_id', $this->hotelId)
                                ->with(['booking' =>  function ($query){
                 $query->where('schedule_id', '!=', $this->eventId);
             }])->get();
         }

        return view('livewire.event-room',[
                'hotels' => $this->hotels,
                'events' => $this->events,
        ]);
    }
}
