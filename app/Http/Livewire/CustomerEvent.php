<?php

namespace App\Http\Livewire;

use App\Models\Schedule;
use Livewire\Component;

class CustomerEvent extends Component
{
    public $events;


    public function render()
    {

        $this->events = Schedule::where('status', 'Active')
            ->whereHas('booking.user', function ($query) {
                $query->where('user_id', auth()->user()->id);
        })->get();

        return view('livewire.customer-event',[
            'events' => $this->events
        ]);
    }
}
