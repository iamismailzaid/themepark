<?php

namespace App\Http\Controllers;


use App\Http\Requests\BookingCreateRequest;
use App\Models\Booking;
use App\Models\Room;
use App\Models\Schedule;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BookingController extends Controller
{

    public function index()
    {
        $bookings = Booking::with(['room','schedule'])->latest()->get();
        return view('themepark.booking.index',compact('bookings'));
    }

    public function create()
    {
        return view('themepark.booking.index');
    }

    public function bookingCreate(Room $room)
    {
        $events  = Schedule::where('status','Active')->get();
        return view('themepark.booking.create', compact('room','events'));
    }

    public function store(BookingCreateRequest $request)
    {

        $user = User::where('email', $request->email)->first();

        //create customer
        if (empty($user)){
            $user = User::create([
                'name'  => $request->name,
                'nic'  => $request->nic,
                'mobile'  => $request->mobile,
                'email' => $request->email,
                'isAdmin'=> 0,
                'password' => md5( Str::random(8))
            ]);
        }

        $room = Room::find($request->room);

        //create booking
        Booking::create([
            'uuid' => Str::random(4),
            'user_id' => $user->id,
            'schedule_id' => $request->event,
            'room_id' => $request->room,
            'total' => $room->price,
        ]);

        return redirect()->route('bookings.index')
                         ->with('success','Booked successfully.');
    }

    public function guestBooking(Schedule $schedule, Room $room){

        $user = Auth::user();
        Booking::create([
            'uuid' => Str::random(4),
            'user_id' => $user->id,
            'schedule_id' => $schedule->id,
            'room_id' => $room->id,
            'total' => $room->price,
        ]);
        return redirect()->route('user.history')
                ->with('success','Booked successfully.');
    }

    public function destroy(Booking $booking)
    {
        $booking->delete();
        return redirect()->route('bookings.index')
            ->with('success','Booking deleted successfully.');
    }
}
