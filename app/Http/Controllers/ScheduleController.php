<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScheduleCreateRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Models\Hotel;
use App\Models\Schedule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    use HasFactory;


    public function index()
    {
        $events = Schedule::latest()->get();
        return view('themepark.schedule.index',compact('events'));
    }


    public function create()
    {
        return view('themepark.schedule.create');
    }

    public function store(ScheduleCreateRequest $request)
    {

        $data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'starting' => $request->starting,
            'due' => $request->due,
            'description' => $request->description,
        ];

        if ($request->has('image')) {
            $data['image'] = request('image')->store('/', 'schedule');
        }

        Schedule::create($data);

        return redirect()->route('schedules.index')
                         ->with('success','Schedule created successfully.');
    }

    public function show(Schedule $schedule)
    {
        $event = $schedule;
        $hotels = Hotel::latest()->paginate(15);
        return view('themepark.schedule.show',compact('hotels','event'));

    }

    public function edit(Schedule $schedule)
    {
        return view('themepark.schedule.edit',compact('schedule'));
    }


    public function update(ScheduleUpdateRequest $request, Schedule $schedule)
    {
        $data = $request->all();

        if ($request->has('image')) {
            $data['image'] = request('image')->store('/', 'schedule');
        }

        $schedule->update($data);

        return redirect()->route('schedules.index')
            ->with('success','Schedule updated successfully.');

    }


    public function destroy(Schedule $schedule)
    {
        $schedule->delete();

        return redirect()->route('schedules.index')
                ->with('success','Schedule deleted successfully.');
    }
}
