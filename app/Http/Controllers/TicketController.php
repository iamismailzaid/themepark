<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Schedule;
use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{

    public function destroy(Ticket $ticket, Booking $booking)
    {
        $total = $booking->total;

        $activityPrice = $ticket->activity->price;

        $booking->update([
            'total' => $total-$activityPrice
        ]);

        $ticket->delete();

        return redirect()->route('user.history')
            ->with('success','Booking updated and ticket deleted successfully.');
    }
}
