<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomCreateRequest;
use App\Http\Requests\RoomUpdateRequest;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoomController extends Controller
{

    public function index()
    {
        $rooms = Room::latest()->get();
        return view('themepark.room.index',compact('rooms'));
    }

    public function store(RoomCreateRequest $request)
    {

        $data = [
            'name' => $request->name,
            'hotel_id' => $request->hotel_id ,
            'number' => $request->number,
            'slug' => Str::slug($request->name),
            'type' => $request->type,
            'price' => $request->price,
        ];

        if ($request->has('image')) {
            $data['image'] = request('image')->store('/', 'room');
        }
        Room::create($data);

        return redirect()->route('rooms.index')
            ->with('success','Room created successfully.');
    }

    public function edit(Room $room)
    {
        return view('themepark.room.edit',compact('room'));
    }


    public function update(RoomUpdateRequest $request, Room $room)
    {

        $data = [
            'name' => $request->name,
            'hotel_id' => $request->hotel_id ,
            'number' => $request->number,
            'slug' => Str::slug($request->name),
            'type' => $request->type,
            'price' => $request->price,
        ];

        if ($request->has('image')) {
            $data['image'] = request('image')->store('/', 'room');
        }

        $room->update($data);

        return redirect()->route('rooms.index')
            ->with('success','Room updated successfully.');
    }


    public function destroy(Room $room)
    {
        $room->delete();
        return redirect()->route('rooms.index')
            ->with('success','Room Deleted successfully.');
    }
}
