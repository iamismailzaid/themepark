<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Booking;
use App\Models\Hotel;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function index(){
        $events = Schedule::where('status','Active')->latest()->paginate(12);
        return view('welcome',compact('events'));
    }
    public function about(){
        return view('themepark.pages.about');
    }

    public function hotel(Request $request){
        $hotels = Hotel::latest()->paginate(12);
       return view('themepark.pages.hotel',compact('hotels'));
    }

    public function activities(Request $request){
        $activities = Activity::all();
        return view('themepark.pages.activities',compact('activities'));
    }

    public function getUserHistory(){
        $user = Auth::user();
        if (!$user->isAdmin){
            //user history
            $bookings = Booking::where('user_id', $user->id)
                ->with(['room','schedule','tickets.activity'])
            ->get();
         return view('themepark.user.history',compact('bookings'));
        }
        //get customer booking with tickets and activity name
        $bookings = Booking::latest()->with(['user','schedule','room'])->get();
        return view('themepark.user.index',compact('bookings'));
    }

    public function setScheduleRoom(Schedule $schedule, Hotel $hotel){
        $event = $schedule;
        $hotel =  $hotel->load('rooms');
        return view('themepark.pages.booking',compact('event','hotel'));
    }


}
