<?php

namespace App\Http\Controllers;

use App\Http\Requests\HotelCreateRequest;
use App\Http\Requests\HotelUpdateRequest;
use App\Models\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HotelController extends Controller
{

    public function index()
    {
        $hotels = Hotel::all();
        return view('themepark.hotel.index',compact('hotels'));
    }


    public function create()
    {
        return view('themepark.hotel.create');
    }

    public function show(Hotel $hotel)
    {
       $rooms = $hotel->rooms;
       return view('themepark.hotel.show',compact('hotel','rooms'));
    }

    public function store(HotelCreateRequest $request)
    {
        $data = [
            'name' => $request->name,
            'location' => $request->location,
            'description' => $request->description,
            'slug' => Str::slug($request->name)
        ];

        if ($request->has('image')) {
            $data['image'] = request('image')->store('/', 'hotel');
        }

        Hotel::create($data);
        return redirect()->route('hotels.index')
                         ->with('success','Hotel created successfully.');
    }

    public function edit(Hotel $hotel)
    {
        return view('themepark.hotel.edit' , compact('hotel'));
    }


    public function update(HotelUpdateRequest $request, Hotel $hotel)
    {
        $data = [
            'name' => $request->name,
            'location' => $request->location,
            'description' => $request->description,
            'slug' => Str::slug($request->name)
        ];

        if ($request->has('image')) {
            $data['image'] = request('image')->store('/', 'hotel');
        }

        $hotel->update($data);
        return redirect()->route('hotels.index')
                         ->with('success','Hotel updated successfully.');
    }

    public function destroy(Hotel $hotel)
    {
        $hotel->delete();
        return redirect()->route('hotels.index')
                         ->with('success','Hotel Deleted successfully.');
    }
}
