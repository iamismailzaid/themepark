<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActivityCreateRequest;
use App\Http\Requests\ActivityUpdateRequest;
use App\Models\Activity;
use App\Models\Booking;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ActivitiesController extends Controller
{

    public function index()
    {
        $activities = Activity::all();
        return view('themepark.activities.index',compact('activities'));
    }

    public function store(ActivityCreateRequest $request)
    {
        $data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'price' => $request->price,
            'description' => $request->description,
        ];
        Activity::create($data);
        return redirect()->route('activities.index')
                         ->with('success','Activity created successfully.');
    }

    public function purchaseTicket(Activity $activity, Request $request )
    {
        $event = $request->event;
        $price = $activity->price;
        $no = $request->amount;
        //get the active bookings
        $bookings = Booking::with(['schedule'=>function($query){
            $query->where('status','Active');
        }])->where('schedule_id',$event)->get();

        //for schedule buy the same ticket
        foreach ($bookings as $booking){
            Ticket::create([
               'booking_id' => $booking->id,
               'activity_id' => $activity->id,
               'tickets' => $no,
            ]);

            //updated the booking total
            $booking->update([
                'total' => $booking->total + ( $price * $no ),
            ]);
        }
        return redirect()->route('user.history')
            ->with('success','Activity added to selected Event and Booking updated successfully.');
    }


    public function edit(Activity $activity)
    {
        return view('themepark.activities.edit',compact('activity'));
    }


    public function update(ActivityUpdateRequest $request, Activity $activity)
    {
        $data = [
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'price' => $request->price,
            'description' => $request->description,
        ];

        $activity->update($data);
        return redirect()->route('activities.index')
            ->with('success','Activity updated successfully.');
    }


    public function destroy(Activity $activity)
    {
        $activity->delete();
        return redirect()->route('activities.index')
             ->with('success','Activity deleted successfully.');
    }
}
