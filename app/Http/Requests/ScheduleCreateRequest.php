<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:schedules,name',
            'image' => 'required|max:1024|image|mimes:jpeg,jpg,png',
            'starting' => 'required|after_or_equal:today',
            'due' => 'required|after:starting',
            'description' => 'required',
        ];
    }
}
