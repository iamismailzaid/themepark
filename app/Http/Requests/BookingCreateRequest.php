<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'nic' => 'required|unique:users,nic',
            'mobile' => 'required',
            'email' => 'required|unique:users,email' ,
            'event' => 'required',
            'room' => 'required',
            'hotel' => 'required',
        ];
    }
}
