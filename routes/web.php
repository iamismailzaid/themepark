<?php

use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HotelController;
use App\Http\Controllers\ActivitiesController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\RoomController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//auth()->login(
//    \App\Models\User::find(1)
//);

Route::group(['middleware' => 'auth'], function (){
    Route::resource('hotels', HotelController::class);
    Route::resource('rooms', RoomController::class);
    Route::resource('schedules', ScheduleController::class);
    Route::resource('bookings', BookingController::class);
    Route::resource('activities', ActivitiesController::class);
    Route::post('/{activity}/buy',[ActivitiesController::class,'purchaseTicket'])->name('buy.tickets');
    Route::get('themepark/activites',[PageController::class,'activities'])->name('themepark.activities');
    Route::get('user/history',[PageController::class, 'getUserHistory'])->name('user.history');
    Route::get('set/{schedule}/{hotel}',[PageController::class,'setScheduleRoom'])->name('set.schedule');
    Route::post('guest/{schedule}/{room}',[BookingController::class, 'guestBooking'])->name('confirm.create');
    Route::delete('tickets/{ticket}/{booking}',[TicketController::class, 'destroy'])->name('tickets.destroy');
});

Route::get('/',[PageController::class,'index'])->name('home');
Route::get('/home',[PageController::class,'index'])->name('home');
Route::get('/about',[PageController::class,'about'])->name('about');
Route::get('/themepark/hotels',[PageController::class,'hotel'])->name('themepark.hotels');

