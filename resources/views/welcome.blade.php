@extends('layouts.app')

@section('content')

    <div class="border p-20">

        <div class="container mx-auto">

                <div class="text-center">
                    <h2 class="text-4xl text-teal-600 font-bold ">Welcome to Theme Park</h2>
                    <p class="leading text-gray-500">Stay home stay safe have a virtual park</p>
                </div>

                <p class="leading flex text-gray-800 font-medium mb-4 border-b text-2xl p-2">
                    Our Upcoming Events
                </p>

                <div class="flex flex-wrap justify-center">
                    @foreach($events as $event)

                        <div class="max-w-sm overflow-hidden shadow-lg border m-2 hover:shadow-xl rounded-lg  duration-300 transform bg-white rounded shadow-sm hover:-translate-y-1">
                            <img class="w-full" style="height:350px"
                                 src="{{ $event->image() }}"
                                 alt="">
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">
                                    <a href="{{$event->path()}}" class="capitalize hover:text-teal-500">
                                        {{$event->name}}

                                    </a>

                                </div>
                                <p class="text-gray-700 text-base">
                                    {{$event->description}}
                                </p>
                            </div>
                            <div class="px-6 pt-4 pb-2 mb-4 ">
                                <span class="bg-green-500 rounded-full px-3 py-1 text-sm font-semibold text-white mb-2">#Themepark</span>
                                <span class="bg-red-600 rounded-full px-3 py-1 text-sm font-semibold text-white mb-2 italic">Closing on {{$event->due->format('j F')}} </span>
                            </div>
                        </div>

                    @endforeach
                </div>
            <div class="p-2">
                {{ $events->links() }}
            </div>
        </div>

    </div>


@endsection
