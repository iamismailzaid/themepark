@props(['route'])

@php
    $classes = Request::is($route) ? 'bg-gray-900' : 'hover:bg-gray-700';
@endphp

<a href="{{ route($route) }}" {{ $attributes->merge(['class' => "ml-4 px-3 py-2 rounded-md text-sm font-medium leading-5 text-gray-300 hover:text-white focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out 
    {$classes}"]) }}>
    {{ $slot }}
</a>