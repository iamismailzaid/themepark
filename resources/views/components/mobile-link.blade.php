
@props(['route'])
    <a href="{{route($route)}}"
       class="block px-3 py-2 rounded-md text-base font-medium text-white focus:outline-none hover:bg-gray-700 focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out">
        {{$slot}}
    </a>
