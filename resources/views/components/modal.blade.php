
<div x-data="{ on: false }" x-on:close-modal.window="on = false" @keydown.escape="on = false" x-cloak >
  <span  @click="on = true">{{ $trigger }}</span>

  <div class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center" x-show="on">
    <div
      class="fixed inset-0 transition-opacity"
      x-show="on"
      x-transition:enter="ease-out duration-200"
      x-transition:enter-start="opacity-0"
      x-transition:enter-end="opacity-100"
      x-transition:leave="ease-in duration-200"
      x-transition:leave-start="opacity-100"
      x-transition:leave-end="opacity-0"
    >
      <div class="absolute inset-0 bg-gray-800 opacity-75"></div>
    </div>

    <div class="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full"
      x-on:click.away="on=false"
      role="dialog"
      aria-modal="true"
      aria-labelledby="modal-headline"
      x-show="on"
      x-transition:enter="ease-out duration-200"
      x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
      x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
      x-transition:leave="ease-in duration-200"
      x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
      x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
    >
      <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
        <div class="sm:flex sm:items-start">
          <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
            <h3 class="text-xl border-b-2 border-gray-400 p-2 capitalize leading-6 font-medium text-gray-800 mb-6"
              id="modal-headline"
            >
              {{ $title ?? 'Hi' }}
            </h3>

            <div class="mt-2">
              <p class="text-sm leading-5 text-gray-500 tracking-wide">
                {{ $slot }}
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="bg-gray-100 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
          {{ $submit }}
        </span>
        <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
          {{ $clear }}
        </span>
      </div>

    </div>
  </div>

</div>
