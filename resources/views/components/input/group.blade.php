@props([
    'label',
    'for',
    'error' => false,
    'helpText' => false,
])

<div class="sm:items-start sm:border-gray-200 p-2">
    <label for="{{ $for }}" class="block font-medium text-sm leading-5 text-gray-700 mb-1">
        {{ $label }}
    </label>

    <div class="mt-1 sm:mt-0 sm:col-span-2">
        {{ $slot }}

        @if ($error)
            <div class="mt-1 text-red-500 text-xs">{{ $error }}</div>
        @endif

        @if ($helpText)
            <p class="mt-2 text-xs text-gray-500">{{ $helpText }}</p>
        @endif
    </div>
</div>
