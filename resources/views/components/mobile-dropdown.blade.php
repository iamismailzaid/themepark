<div @click.away="open = false" class="relative" x-data="{ open: false }">

    <button @click="open = !open"
            class="mt-1 block px-3 py-2 rounded-md text-base font-medium
            text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none
            focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out">

        <a>{{$trigger}}</a>

        <svg fill="currentColor" viewBox="0 0 20 20"
             :class="{'rotate-180': open, 'rotate-0': !open}"
             class="inline w-4 h-4 mt-1 ml-1 transition-transform duration-200 transform md:-mt-1">

            <path fill-rule="evenodd"
                  d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                  clip-rule="evenodd">
            </path>
        </svg>

    </button>

    <div x-show="open"
         x-transition:enter="transition ease-out duration-100"
         x-transition:enter-start="transform opacity-0 scale-95"
         x-transition:enter-end="transform opacity-100 scale-100"
         x-transition:leave="transition ease-in duration-75"
         x-transition:leave-start="transform opacity-100 scale-100"
         x-transition:leave-end="transform opacity-0 scale-95"
         class="absolute right-0 w-full mt-2 origin-top-right rounded-md shadow-lg" style="display: none;">

        <div class="py-1 rounded-md bg-white shadow-xs">

            {{$slot}}

        </div>
    </div>
</div>