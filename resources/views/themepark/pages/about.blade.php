@extends('layouts.app')

@section('content')
<section class="text-gray-700 body-font">
    <div class="text-left p-10">
        <h2 class="font-bold capitalize border-b text-3xl">About Us</h2>
    </div>
    <div class="container mx-auto flex px-5 py-5 items-center justify-center flex-col">
        <div class="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded" alt="hero" >
            <x-logo></x-logo>
        </div>
        <div class="text-center lg:w-2/3 w-full">
            <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Theme Park</h1>
            <p class="mb-8 leading-relaxed">
                This assignment is based on the following hypothetical concept:
                Theme Park Island, is a theme park created in the nearby island Kuda Bandos, by HDC.
                Every weekend, the island is open for public. On special occasions, the island will be opened
                too.
                Families have to book rooms and go to that island in Ferry, which is provided free, for
                customers with valid room booking.
                In that island, families have following activities that they can participate in:
            </p>
            <div class="flex justify-center">
                <div class="text-white bg-indigo-500
                capitalize border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-left text-lg">
                    <p>Batch Number: BIT-SD-A4</p>
                    <p>Due: 11 Nov 2020</p>
                    <p>Lecture: Hussain Naushadh </p>
                    <p>Student: Ismail Zaid </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection