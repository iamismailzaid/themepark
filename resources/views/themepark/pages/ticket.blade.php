@extends('layouts.app')

@section('content')

    <div class="border p-20">

        <div class="container mx-auto">

            <div class="text-center">

                <h2 class="text-4xl text-teal-600 font-bold">
                    Activities You Enjoy
                </h2>
                <p class="leading text-gray-500">Winner get rewards</p>
            </div>

            <div class=" py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                @foreach($activites as $activity)
                <div class="grid gap-8 row-gap-5 lg:grid-cols-3">
                    <div class="relative p-px overflow-hidden transition duration-300 transform border rounded shadow-sm hover:scale-105 group hover:shadow-xl">
                        <div class="absolute bottom-0 left-0 w-full h-1 duration-300 origin-left transform scale-x-0 bg-deep-purple-accent-400 group-hover:scale-x-100"></div>
                        <div class="absolute bottom-0 left-0 w-1 h-full duration-300 origin-bottom transform scale-y-0 bg-deep-purple-accent-400 group-hover:scale-y-100"></div>
                        <div class="absolute top-0 left-0 w-full h-1 duration-300 origin-right transform scale-x-0 bg-deep-purple-accent-400 group-hover:scale-x-100"></div>
                        <div class="absolute bottom-0 right-0 w-1 h-full duration-300 origin-top transform scale-y-0 bg-deep-purple-accent-400 group-hover:scale-y-100"></div>
                        <div class="relative p-5 bg-white rounded-sm">
                            <div class="flex flex-col mb-2 lg:items-center lg:flex-row">
                                <h6 class="font-semibold leading-5">{{ $activity->name }}</h6>
                            </div>
                            <p class="mb-2 text-sm text-gray-900">
                                {{ $activity->description }}
                            </p>
                            <a href="/" aria-label="" class="inline-flex items-center text-sm font-semibold transition-colors duration-200 text-deep-purple-accent-400 hover:text-deep-purple-800">Learn more</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>

@endsection

