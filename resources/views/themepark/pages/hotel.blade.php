@extends('layouts.app')

@section('content')

    <div class="border p-20">

        <div class="container mx-auto">

            <div class="text-center">

                <h2 class="text-4xl text-teal-600 font-bold">
                    It's bad manners to keep a vacation waiting
                </h2>
                <p class="leading text-gray-500">Living the suite life</p>
            </div>

            <p class="leading flex border-b text-gray-800 font-medium mb-4 text-2xl p-2">
                Available Hotels
            </p>

            <div class="flex flex-wrap mb-10 justify-center">

                @foreach($hotels as $hotel)

                    <div class="max-w-sm overflow-hidden shadow-lg border m-2 hover:shadow-xl rounded-lg  duration-300 transform bg-white rounded shadow-sm hover:-translate-y-1">
                        <img class="w-full object-cover" style="height:350px; width: 470px"
                             src="{{ $hotel->image() }}"
                             alt="">
                        <div class="px-6 py-4">
                            <div class="font-bold text-xl mb-2">
                                <a href="{{route('hotels.show',$hotel->slug)}}"
                                   class="capitalize hover:text-teal-500">
                                    {{$hotel->name}}
                                </a>
                            </div>
                            <p class="text-gray-700 text-base">
                                {{$hotel->description}}
                            </p>
                        </div>
                    </div>

                @endforeach


            </div>
            {{ $hotels->links() }}
        </div>
    </div>

@endsection

