@extends('layouts.app')

@section('content')

    <div class="border p-5">

        <div class="container mx-auto">

            <div class="text-center">

                <h2 class="text-4xl text-teal-600 font-bold">
                    Activities You May Enjoy
                </h2>
                <p class="leading text-gray-500">Winner gets nothing </p>
            </div>
            <div class="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                <div class="grid gap-8 row-gap-5 lg:grid-cols-3">

                    @foreach($activities as $activity)
                    <div class="relative p-px overflow-hidden transition duration-300 transform border rounded shadow-sm hover:scale-105 group hover:shadow-xl">
                        <div class="relative p-5 bg-white rounded-sm">
                            <div class="flex flex-col mb-2 lg:items-center lg:flex-row">
                                <h6 class="text-xl font-medium border-b border-gray-900 p-2 leading-5 capitalize">{{$activity->name}}</h6>
                            </div>
                            <p class="mb-2 text-sm text-gray-700">
                                {{ $activity->description }}
                            </p>
                        </div>
                        <div class="p-2 shadow">
                            <form method="post" action="/{{$activity->slug}}/buy">
                                @csrf
                                {{-- auth users activity --}}
                                <livewire:customer-event />

                                <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                    <div class="flex -mr-px">
                                        <span class="flex items-center leading-normal bg-gray-400 rounded rounded-r-none border border-r-0 border-grey-light px-3 whitespace-no-wrap text-grey-dark text-sm">
                                            No of ticket
                                        </span>
                                    </div>
                                    <input type="number"
                                           required
                                           name="amount"
                                           class="outline-none flex-shrink flex-grow flex-auto leading-normal w-px flex-1 border h-10 border-grey-light
                                     rounded rounded-l-none px-3 relative focus:border-blue focus:shadow text-gray-600"
                                           min="1" max="5">
                                </div>

                                <button class="bg-green-400 px-2 py-2 rounded hover:bg-green-500 text-sm hover:text-white">Buy Now</button>
                            </form>
                        </div>

                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>



@endsection

