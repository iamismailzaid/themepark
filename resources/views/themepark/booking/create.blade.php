@extends('layouts.app')

@section('content')


    <div class="h-screen w-full flex items-center justify-center bg-teal-lightest font-sans">
        <div class="h-screen w-full absolute flex items-center justify-center bg-modal">
            <div class="">
                <div class="mb-4">
                    <h1>Welcome!</h1>
                </div>
                <div class="">
                    <p>Ready to get started? Keep scrolling to see some great components.</p>
                </div>
                <div class="flex justify-center">
                    <button class="flex-no-shrink text-white py-2 px-4 rounded bg-teal hover:bg-teal-dark">Let's Go</button>
                </div>
            </div>
        </div>
    </div>


@endsection
