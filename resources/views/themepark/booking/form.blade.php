
    <label class="block m-2">
        <span class="text-gray-700 font-semibold text-sm">Customer name</span>
        <input  class="text-gray-700  p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full
            outline-none
            @error ('name') border-red-500 @enderror"
                placeholder="Enter Customer name.."
                name="name"
                required
        >

        @error('name')
        <span class="text-red-500 text-xs">{{$message}}</span>
        @enderror
    </label>

<label class="block m-2">
    <span class="text-gray-700 font-semibold text-sm">Email</span>
    <input  class="text-gray-700  p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full
            outline-none
            @error ('email') border-red-500 @enderror"
            placeholder="Enter Customer email.."
            name="email"
            required
            value="{{old('email')}}"
    >

    @error('email')
    <span class="text-red-500 text-xs">{{$message}}</span>
    @enderror
</label>

    <label class="block m-2">
        <span class="text-gray-700 font-semibold text-sm">NIC(ex-A00123)</span>
        <input  class="text-gray-700 p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full
            outline-none
            @error ('nic') border-red-500 @enderror"
                placeholder="Enter hotel name.."
                name="nic"
                required
                value="{{old('nic')}}"
        >

        @error('nic')
        <span class="text-red-500 text-xs">{{$message}}</span>
        @enderror
    </label>

    <label class="block m-2">
        <span class="text-gray-700 font-semibold text-sm">Mobile</span>
        <input  class="text-gray-700 p-2 border rounded text-sm
                form-input mt-1 rounded-lg block w-full
                outline-none
                @error ('name') border-red-500 @enderror"
                placeholder="mobile 7777070.."
                name="mobile"
                type="number"
                required
                value="{{old('number')}}"
        >

        @error('number')
        <span class="text-red-500 text-xs">{{$message}}</span>
        @enderror
    </label>

<livewire:event-room />





