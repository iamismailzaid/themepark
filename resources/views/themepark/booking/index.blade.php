@extends('layouts.app')

@section('content')
    <div class="">
        <div x-data ="{open:false}" x-cloak class="container mx-auto p-2">
            <div class="text-left">
                <h2 class="font-bold capitalize border-b text-3xl">Bookings</h2>
            </div>
            <button @click="open = true" x-show="!open" class="bg-green-500 px-3 py-1 m-2
                    text-sm rounded hover:text-white hover:bg-green-400">
                New Booking
            </button>

            <div x-show="open" class="h-screen w-full flex flex-col items-center justify-center font-sans">
                <div class="w-full absolute flex items-center justify-center">
                    <div class="bg-white rounded-lg shadow-2xl border p-8 m-4 w-1/2 overflow-y-scroll">
                        <div class="mb-2">
                            <h1 class="font-medium text-2xl text-center">Make New Booking</h1>
                        </div>
                        <div class="mb-8">
                            <form autocomplete="off" action="{{ route('bookings.store') }}" method="POST">
                                @csrf
                                @include('themepark.booking.form')

                            <div class="mt-5 sm:px-6 sm:flex sm:flex-row-reverse">
                                <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                                    <button type="submit"
                                    class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-blue-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-blue-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                        Create
                                    </button>
                                </span>
                                <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                                    <a href="{{route('bookings.index')}}">
                                        <button @click="open = false; setTimeout(() => open = true, 1000)"
                                                type="submit"
                                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                            Cancel
                                        </button>
                                    </a>
                                </span>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div x-show="!open"  class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                    <table class="min-w-full">
                        <thead>
                        <tr>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">#</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Room</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Room Type</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Schedule</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Total</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Created_at</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                            @forelse ($bookings as $booking)

                                <tr class="border-b border-gray-200 bg-white text-sm text-gray-800">
                                    <td class="border text-center p-2"> {{$loop->iteration}}</td>
                                    <td class="border text-center p-2"> {{$booking->room->name}}</td>
                                    <td class="border text-center p-2"> {{$booking->room->type}}</td>
                                    <td class="border text-center p-2"> {{$booking->schedule->starting->toDateString()}}</td>
                                    <td class="border text-center p-2"> {{number_format($booking->total,2)}}</td>
                                    <td class="border text-center p-2"> {{$booking->created_at->diffForHumans()}}</td>
                                    <td class="border text-center p-2">

                                        <div class="flex justify-center">

                                            <x-modal title="confirmation">

                                                <x-slot name="trigger">
                                                    <button class="bg-red-400 px-3 py-1 mr-2 text-sm rounded hover:text-white hover:bg-red-500">
                                                        Delete
                                                    </button>
                                                </x-slot>

                                                <span class="text-centre text-gray-500">Are you sure you want to delete this booking? </span>

                                                <x-slot name="submit">
                                                    <form action="{{route('bookings.destroy', $booking->id )}}" method="POST">
                                                        @method('DELETE')
                                                        @csrf

                                                        <button type="submit"
                                                                class=" text-sm bg-red-600 text-white hover:bg-red-400 p-2 rounded">
                                                            Delete
                                                        </button>
                                                    </form>
                                                </x-slot>

                                                <x-slot name="clear">
                                                    <a  @click="on = false"
                                                        class="inline-flex cursor-pointer justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                                    >
                                                        Cancel
                                                    </a>
                                                </x-slot>
                                            </x-modal>

                                        </div>
                                    </td>

                                </tr>
                            @empty
                                <p class="text-center bg-red-500 text-white leading-5 p-2 border shadow-lg">
                                    No bookings added!
                                </p>

                            @endforelse
                        </tbody>
                    </table>
                    <div class="px-5 py-5 bg-white border-t flex flex-col xs:flex-row items-center xs:justify-between">
                <span class="text-xs xs:text-sm text-gray-900">

                </span>
                        <div class="inline-flex mt-2 xs:mt-0">
                            {{-- {{$brands->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
