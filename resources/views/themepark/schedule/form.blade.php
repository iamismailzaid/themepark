
<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Schedules name</span>

    <input  class="text-gray-700 p-2 border rounded text-sm @error ('name') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            placeholder="Enter hotel name.."
            name="name"
            required
            value = "{{old('name')}}"

    >
</label>

@error('name')
<span class="text-red-500 text-xs">{{$message}}</span>
@enderror

<label class="block p-2">
    <span class="text-gray-700 text-sm">Select Event Status</span>
    <select name="country_id" class="text-gray-700 p-2 border text-sm @error ('country_id') border-red-500 @enderror form-input mt-1 rounded-lg block w-full">
        <option value="Active">Opened</option>
        <option value="Closed">Closed</option>
    </select>
</label>

<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Event Poster</span>

    <input  class="text-gray-700 p-2 border rounded text-sm @error ('image') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            name="image"
            type="file"
            value="{{old('image')}}}"
            @if($route!='schedules.edit')
            required
            value="{{old('image')}}}"
            @endif
    >
</label>

@error('image')
<span class="text-red-500 text-xs">{{$message}}</span>
@enderror

<x-input.group label="Starting date" for="starting" :error="$errors->first('starting')">
    <x-input.date value="{{old('starting')}}" name="starting" id="starting" placeholder="MM/DD/YYYY"/>
</x-input.group>

<x-input.group label="Due date" for="due" :error="$errors->first('due')">
    <x-input.date value="{{old('due')}}" name="due" id="due" placeholder="MM/DD/YYYY"/>
</x-input.group>


<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Description</span>
    <textarea class=" text-gray-700 p-2 border rounded text-sm @error ('description') border-red-500 @enderror  form-textarea rounded-lg mt-1 block w-full"
              rows="3"
              name="description"
              placeholder="Enter some detail"
              required
    >
            {{old('description')}}
        </textarea>

</label>


