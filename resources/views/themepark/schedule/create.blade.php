@extends('layouts.app')

@section('content')
<div class="bg-gray-300">
    <div x-data="{ open: true }" x-show="open" class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex  sm:items-center sm:justify-center">
                <div x-show="open" x-description="Background overlay, show/hide based on modal state."
                     x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0 transition-opacity">
                    <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
                </div>

                <div x-show="open" x-description="Modal panel, show/hide based on modal state."
                     x-transition:enter="ease-out duration-300"
                     x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                     x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                     x-transition:leave="ease-in duration-200"
                     x-transition:lea ve-start="opacity-100 translate-y-0 sm:scale-100"
                     x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                     class="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-xl sm:w-full"
                     role="dialog"
                     aria-modal="true"
                     aria-labelledby="modal-headline">

                    <div class="bg-white px-4 pt-5 pb-4 sm:pb-4">
                        <div>
                            <form autocomplete="off" action="{{route('schedules.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="mt-3 mb-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                    <h3 class="text-lg text-center leading-6 font-medium text-gray-900">
                                        Create a Event
                                    </h3>

                                    {{-- create form --}}
                                    <label class="block p-2">
                                        <span class="text-gray-700 font-semibold text-sm">Schedules name</span>

                                        <input  class="text-gray-700 p-2 border rounded text-sm @error ('name') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
                                                placeholder="Enter hotel name.."
                                                name="name"
                                                required
                                                value = "{{old('name')}}"
                                        >
                                    </label>

                                    @error('name')
                                    <span class="text-red-500 text-xs">{{$message}}</span>
                                    @enderror

                                    <label class="block p-2">
                                        <span class="text-gray-700 text-sm">Select Event Status</span>
                                        <select name="status" class="text-gray-700 p-2 border text-sm @error ('status') border-red-500 @enderror form-input mt-1 rounded-lg block w-full">
                                            <option value="Active">Opened</option>
                                            <option value="Closed">Closed</option>
                                        </select>
                                    </label>

                                    <label class="block p-2">
                                        <span class="text-gray-700 font-semibold text-sm">Event Poster</span>

                                        <input  class="text-gray-700 p-2 border rounded text-sm @error ('image') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
                                                name="image"
                                                required
                                                type="file"
                                                value="{{ old('image') }}"
                                        >
                                    </label>

                                    @error('image')
                                    <span class="text-red-500 text-xs">{{$message}}</span>
                                    @enderror

                                    <x-input.group label="Starting date" for="starting" :error="$errors->first('starting')">
                                        <x-input.date value="{{old('starting')}}" name="starting" id="starting" placeholder="MM/DD/YYYY"/>
                                    </x-input.group>

                                    <x-input.group label="Due date" for="due" :error="$errors->first('due')">
                                        <x-input.date value="{{old('due')}}" name="due" id="due" placeholder="MM/DD/YYYY"/>
                                    </x-input.group>


                                    <label class="block p-2">
                                        <span class="text-gray-700 font-semibold text-sm">Description</span>
                                        <textarea class=" text-gray-700 p-2 border rounded text-sm @error ('description') border-red-500 @enderror  form-textarea rounded-lg mt-1 block w-full"
                                                  rows="3"
                                                  name="description"
                                                  placeholder="Enter some detail"
                                                  required
                                        >
                                {{old('description')}}
                            </textarea>

                                    </label>
                                    {{-- end create form --}}
                                </div>

                                <div class="px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button type="submit"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-blue-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-blue-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                           Create
                        </button>
                    </span>
                                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">

                        <a href="{{route('schedules.index')}}">
                            <button @click="open = false; setTimeout(() => open = true, 1000)" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                Cancel
                            </button>
                        </a>
                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
</div>
</div>
@endsection
