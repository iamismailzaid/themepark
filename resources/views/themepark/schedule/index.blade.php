@extends('layouts.app')

@section('content')

    <div class="border p-10">
        <div class="container mx-auto">

            <div class="text-left">
                <h2 class="font-bold p-2 capitalize border-b text-3xl">Schedule Events</h2>
            </div>

            <div class="flex m-2">

                <a href="{{ route('schedules.create') }}"
                  class="bg-green-400 px-3 py-1 mr-2 text-sm rounded hover:bg-green-500 hover:text-white">
                    Create
                </a>

            </div>

            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                    <table class="min-w-full ">
                        <thead>
                        <tr>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">#</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Name</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Status</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">starting</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">due_date</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Description</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Updated</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($events as $event)

                            <tr class="border-b border-gray-200 bg-white text-sm text-gray-800">
                                <td class="border text-center "> {{$loop->iteration}}</td>
                                <td class="border text-center "> {{$event->name}}</td>
                                <td class="border text-center {{ $event->status == 'Closed'  ? 'text-red-800' : 'font-bold text-green-400'}}">
                                    {{$event->status}}
                                </td>
                                <td class="border text-center "> {{$event->starting->format('D, d-M ') }}</td>
                                <td class="border text-center "> {{$event->due->format('D, d-M ')  }}</td>
                                <td class="border text-center "> {{$event->description}}</td>
                                <td class="border text-center "> {{$event->updated_at->diffForHumans()}}</td>
                                <td class="border py-2">

                                    <div class="flex justify-center p-1">
                                        <a href="{{route('schedules.edit', $event->slug )}}"
                                           class="bg-orange-400 px-3 py-1 text-sm mr-2 rounded hover:bg-orange-500 hover:text-white">
                                            Edit
                                        </a>


                                        <x-modal title="confirmation">
                                            <x-slot name="trigger">
                                                <button class="bg-red-400 px-3 py-1 mr-2 text-sm rounded hover:bg-red-500 hover:text-white">
                                                    Delete
                                                </button>
                                            </x-slot>

                                            <span class="text-centre text-gray-700">Are you sure you want to delete this schedule? </span>

                                            <x-slot name="submit">
                                                <form action="{{route('schedules.destroy', $event->slug )}}" method="POST">
                                                    @method('DELETE')
                                                    @csrf

                                                    <button type="submit"
                                                            class=" text-sm bg-red-600 text-white hover:bg-red-400 p-2 rounded">
                                                        Delete
                                                    </button>

                                                </form>
                                            </x-slot>

                                            <x-slot name="clear">
                                                <a  @click="on = false"
                                                    class="inline-flex cursor-pointer justify-center w-full rounded-md border
                                                    border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700
                                                    shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue
                                                    transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                                >
                                                    Cancel
                                                </a>
                                            </x-slot>
                                        </x-modal>

                                    </div>
                                </td>
                            </tr>
                        @empty
                            <p class="text-center bg-red-500 text-white leading-5 p-2 border shadow-lg">
                                No schedule added!
                            </p>

                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
