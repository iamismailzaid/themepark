
@php
    $route = Route::currentRouteName();
@endphp

<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Name</span>
    <input  class="text-gray-700 p-2 border rounded text-sm @error ('name') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            placeholder="Enter hotel name.."
            name="name"
            required
            @if($route==='rooms.edit')
            value = "{{$room->name}}"
            @else
            value = "{{old('name')}}"
            @endif
    >
</label>

@error('name')
<span class="text-red-500 text-xs">{{$message}}</span>
@enderror

<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Room Poster</span>

    <input  class="text-gray-700 p-2 border rounded text-sm @error ('image') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            name="image"
            type="file"
            @if($route!='rooms.edit')
            required
            value="{{old('image')}}}"
            @endif
    >
</label>

<label class="block m-2">
    <span class="text-gray-700 font-semibold text-sm">Select Hotel</span>
    {{--   select the hotel --}}
    <select name="hotel_id" class="text-gray-700 p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full">
        @php
            use App\Models\Hotel;
            $hotels = Hotel::orderBy('name')->get();
        @endphp

        @foreach ($hotels as $hotel)
            <option value="{{$hotel->id}}"> {{$hotel->name}} </option>
        @endforeach

    </select>
</label>
    @error('hotel')
        <span class="text-red-500 text-xs">{{$message}}</span>
    @enderror

<label class="block m-2">
    <span class="text-gray-700 font-semibold text-sm">Room Type</span>
    <select name="type" class="text-gray-700 p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full">
        <option value="Single"> Single </option>
        <option value="Double"> Double </option>
        <option value="Triple"> Triple  </option>
        <option value="Queen"> Queen </option>
    </select>

</label>

<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Room Number</span>
    <input  class="text-gray-700 p-2 border rounded text-sm @error ('number') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            placeholder="Enter room number.."
            name="number"
            required
            @if($route==='rooms.edit')
            value = "{{$room->number}}"
            @else
            value = "{{old('number')}}"
            @endif
    >
</label>

@error('number')
<span class="text-red-500 text-xs">{{$message}}</span>
@enderror


<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Price</span>
    <input  class="text-gray-700 p-2 border rounded text-sm @error ('price') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            placeholder="Enter price..."
            name="price"
            type="number"
            required
            @if($route==='rooms.edit')
            value = "{{$room->price}}"
            @else
            value = "{{old('price')}}"
            @endif
    >
    @error('price')
    <span class="text-red-500 text-xs">{{$message}}</span>
    @enderror

</label>


