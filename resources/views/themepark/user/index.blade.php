@extends('layouts.app')

@section('content')
    <div class="">
        <div class="container mx-auto p-5">
            <div class="text-left">
                <h2 class="font-bold capitalize border-b text-3xl">Users History</h2>
            </div>
            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                    <table class="min-w-full">
                        <thead>
                        <tr>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">#</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Customer</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Event Name</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Status</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Room</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">RoomType</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Total</th>
                        </tr>
                        </thead>
                        <tbody>

                            @forelse ($bookings as $booking)

                                <tr class="border-b border-gray-200 bg-white text-sm text-gray-800">
                                    <td class="border text-center p-2"> {{$loop->iteration}}</td>
                                    <td class="border text-center p-2"> {{$booking->user->name}}</td>
                                    <td class="border text-center p-2"> {{$booking->schedule->name}}</td>
                                    <td class="border text-center p-2"> {{$booking->schedule->status}}</td>
                                    <td class="border text-center p-2"> {{$booking->room->name}}</td>
                                    <td class="border text-center p-2"> {{$booking->room->type}}</td>
                                    <td class="border text-center p-2"> MVR {{number_format($booking->total,2) }}</td>

                                </tr>
                            @empty
                                <p class="text-center bg-red-500 text-white leading-5 p-2 border shadow-lg">
                                    No bookings added!
                                </p>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="px-5 py-5 bg-white border-t flex flex-col xs:flex-row items-center xs:justify-between">
                <span class="text-xs xs:text-sm text-gray-900">

                </span>
                        <div class="inline-flex mt-2 xs:mt-0">
                            {{-- {{$brands->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
