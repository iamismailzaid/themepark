@extends('layouts.app')

@section('content')
    <section class="text-gray-700 body-font overflow-hidden">
        <div class="text-left p-10">
            <h2 class="font-bold capitalize text-3xl">History</h2>
            <hr>
        </div>
    <div class="container px-5 py-10 mx-auto">

        <div class="-my-8">
            @foreach($bookings as $booking)
            <div class="py-8 flex flex-wrap md:flex-no-wrap border p-4 rounded">
                <div class="md:w-64 md:mb-0 mb-6 flex-shrink-0 flex flex-col">
                    <span class="tracking-widest font-medium title-font text-gray-900">{{$booking->room->name}}| {{$booking->room->type}}</span>
                    <span class="mt-1 text-gray-500 text-sm">{{$booking->schedule->starting->format('D, d-M-Y')}}</span>
                    <span class="mt-1 text-green-500 text-sm">{{$booking->schedule->status}}</span>

                </div>
                <div class="md:flex-grow">
                    <h2 class="text-2xl font-medium text-gray-900 title-font mb-2">
                        {{$booking->schedule->name}}
                    </h2>
                    <p class="leading-relaxed">
                    {{$booking->schedule->description}}
                    </p>
                    <br>
                    <span class="tracking-widest font-medium title-font text-gray-900">Ticket History</span>

                    @foreach($booking->tickets as $ticket)
                        <div class="flex justify-between border p-2 rounded my-1">
                            <span class="mt-1 text-gray-800 text-sm leading-5">Activity {{$ticket->activity->name}}</span>
                            <span class="mt-1 text-gray-800 text-sm leading-5">Count {{$ticket->tickets}}</span>

                            <x-modal title="confirmation">

                                <x-slot name="trigger">
                                    <button class="bg-red-400 px-3 py-1 mr-2 text-sm rounded hover:text-white hover:bg-red-500">
                                        Delete
                                    </button>
                                </x-slot>

                                <span class="text-centre text-gray-500">Are you sure you want to delete this hotel? </span>

                                <x-slot name="submit">
                                    <form action="{{route('tickets.destroy', [$ticket->id , $booking->id] )}}" method="POST">
                                        @method('DELETE')
                                        @csrf

                                        <button type="submit"
                                                class=" text-sm bg-red-600 text-white hover:bg-red-400 p-2 rounded">
                                            Delete
                                        </button>
                                    </form>
                                </x-slot>

                                <x-slot name="clear">
                                    <a  @click="on = false"
                                        class="inline-flex cursor-pointer justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                    >
                                        Cancel
                                    </a>
                                </x-slot>
                            </x-modal>
                        </div>

                    @endforeach
                    <hr>
                    <span class="tracking-widest font-medium title-font text-teal-400">Total: MVR {{number_format($booking->total,2)}}</span>
                </div>

            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection