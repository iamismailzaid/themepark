@extends('layouts.app')

@section('content')

    <div class="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">

        <a class="bg-teal-300 hover:bg-teal-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
           href="{{ route('themepark.hotels') }}">
            <span>Back</span>
        </a>

        <div class="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
            <div>
                <p class="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-teal-900 uppercase rounded-full bg-teal-accent-400">
                    ThemePark
                </p>
            </div>
            <h2 class="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto ">
                {{$hotel->name}}
            </h2>
            <p class="text-base text-gray-700 md:text-lg">
                {{$hotel->description}}
            </p>
        </div>
        <div class="mx-auto">
            <div class="relative w-full transition-shadow duration-300 hover:shadow-xl">
                <img class="object-cover  w-full rounded shadow-lg sm:h-64 md:h-80 lg:h-96"
                     src="{{ $hotel->image() }}"
                     alt=""
                />

            </div>
        </div>

{{--    divider    --}}

        <ddiv class="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
            <div>
                <p class="inline-block px-3 py-px mb-4 text-2xl font-semibold tracking-wider text-teal-900 capitalize rounded-full bg-teal-accent-400">
                    Available Rooms
                </p>
            </div>

            <div class="flex flex-wrap mb-10 justify-center">

                @foreach($rooms as $room)

                    <div class="max-w-sm overflow-hidden shadow-lg border m-2 hover:shadow-xl rounded-lg  duration-300 transform bg-white rounded shadow-sm hover:-translate-y-1">
                        <img class="w-full object-cover" style="height:350px; width: 470px"
                             src="{{$room->image()}}"
                             alt="">
                        <div class="px-6 py-4">
                            <div class="font-bold text-gray-700 text-xl mb-2 hover:text-teal-500">
                                <a class="capitalize ">
                                    {{$room->name}} | {{$room->type}}
                                </a>
                            </div>
                            <p class="text-gray-700 text-base">
                                {{$room->description}}
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </ddiv>

    </div>

@endsection

