@extends('layouts.app')

@section('content')

    <div class="border">
        <div class="container mx-auto">

            <div class="text-left">
                <h2 class="font-bold p-2 capitalize border-b text-3xl">Hotels</h2>
            </div>

            <div class="flex m-2">

                <x-modal title="Create Hotels">
                    <x-slot name="trigger">
                        <button class="bg-green-400 px-3 py-1 mr-2 text-sm rounded hover:text-white hover:bg-green-500">
                            Create
                        </button>
                    </x-slot>

                    <form autocomplete="off" action="{{route('hotels.store')}}"
                          method="POST" enctype="multipart/form-data">
                        @csrf
                        @include('themepark.hotel.form')

                        <button type="submit"
                                class=" text-sm bg-blue-600 mt-2 text-white hover:bg-blue-400 py-2 px-3 rounded">
                            Save
                        </button>

                    </form>

                    <x-slot name="submit">

                    </x-slot>


                    <x-slot name="clear">
                        <a  @click="on = false"
                            class="inline-flex cursor-pointer justify-center w-full rounded-md border px-4 py-2 bg-red-500 text-base
                            leading-6 font-medium text-gray-700 shadow-sm hover:text-white focus:outline-none focus:border-blue-300
                            focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                        >
                            Cancel
                        </a>
                    </x-slot>

                </x-modal>

            </div>

            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                    <table class="min-w-full ">
                        <thead>
                        <tr>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">#</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Name</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Location</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Description</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Created_at</th>
                            <th class="px-5 p-2 border-b-2 border-teal-400 bg-teal-300 text-center text-xs text-gray-800">Action</th>

                        </tr>
                        </thead>
                        <tbody>

                        @forelse ($hotels as $hotel)

                            <tr class="border-b border-gray-200 bg-white text-sm text-gray-800">
                                <td class="border text-center p-2"> {{$loop->iteration}}</td>
                                <td class="border text-center p-2"> {{$hotel->name}}</td>
                                <td class="border text-center p-2"> {{$hotel->location}}</td>
                                <td class="border text-center p-2"> {{$hotel->description}}</td>
                                <td class="border text-center p-2"> {{$hotel->created_at->diffForHumans()}}</td>
                                <td class="border text-center p-2">

                                    <div class="flex justify-center">
                                        {{--edit form--}}
                                        <a href="{{ route('hotels.edit', $hotel->slug)}}"
                                           class="bg-indigo-400 px-3 py-1 mr-2 text-sm rounded hover:text-white hover:bg-indigo-700">
                                            Edit
                                        </a>

                                        <x-modal title="confirmation">

                                            <x-slot name="trigger">
                                                <button class="bg-red-400 px-3 py-1 mr-2 text-sm rounded hover:text-white hover:bg-red-500">
                                                    Delete
                                                </button>
                                            </x-slot>

                                            <span class="text-centre text-gray-500">Are you sure you want to delete this hotel? </span>

                                            <x-slot name="submit">
                                                <form action="{{route('hotels.destroy', $hotel->slug )}}" method="POST">
                                                    @method('DELETE')
                                                    @csrf

                                                    <button type="submit"
                                                            class=" text-sm bg-red-600 text-white hover:bg-red-400 p-2 rounded">
                                                        Delete
                                                    </button>
                                                </form>
                                            </x-slot>

                                            <x-slot name="clear">
                                                <a  @click="on = false"
                                                    class="inline-flex cursor-pointer justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5"
                                                >
                                                    Cancel
                                                </a>
                                            </x-slot>
                                        </x-modal>

                                    </div>
                                </td>

                            </tr>
                        @empty
                            <p class="text-center bg-red-500 text-white leading-5 p-2 border shadow-lg">
                                No hotel added!
                            </p>

                        @endforelse

                        </tbody>
                    </table>
                    <div class="px-5 py-5 bg-white border-t flex flex-col xs:flex-row items-center xs:justify-between">
                <span class="text-xs xs:text-sm text-gray-900">

                </span>
                        <div class="inline-flex mt-2 xs:mt-0">
                            {{-- {{$brands->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
