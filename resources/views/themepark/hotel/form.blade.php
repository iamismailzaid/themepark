
@php
    $route = Route::currentRouteName();
@endphp

    <label class="block p-2">
        <span class="text-gray-700 font-semibold text-sm">Hotel name</span>
        <input  class="text-gray-700 p-2 border rounded text-sm @error ('name') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
                placeholder="Enter hotel name.."
                name="name"
                required
            @if($route==='hotels.edit')
                value = "{{$hotel->name}}"
            @else
                value = "{{old('name')}}"
            @endif
        >
    </label>

    @error('name')
    <span class="text-red-500 text-xs">{{$message}}</span>
    @enderror

    <label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Hotel Banner</span>

    <input  class="text-gray-700 p-2 border rounded text-sm @error ('image') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            name="image"
            required
            type="file"
            value="{{ old('image') }}"
    >
    </label>

    @error('image')
    <span class="text-red-500 text-xs">{{$message}}</span>
    @enderror

    <label class="block p-2">
        <span class="text-gray-700 font-semibold text-sm">Location</span>
        <input  class="text-gray-700 p-2 border rounded text-sm @error ('location') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
                placeholder="Enter location.."
                name="location"
                required
            @if($route==='hotels.edit')
                value ="{{$hotel->location }}"
            @else
                value ="{{old('location')}}"
            @endif
        >
    </label>


    @error('location')
    <span class="text-red-500 text-xs">{{$message}}</span>
    @enderror

    <label class="block p-2">
        <span class="text-gray-700 font-semibold text-sm">Description</span>
        <textarea class="text-gray-700 p-2 border rounded text-sm @error ('description') border-red-500 @enderror  form-textarea rounded-lg mt-1 block w-full"
                  rows="3"
                  name="description"
                  placeholder="Enter some detail"
                  required
        >
         @if($route==='hotels.edit')
            {{$hotel->description}}
         @else
            {{old('description')}}
         @endif

        </textarea>
    </label>



