
@php
    $route = Route::currentRouteName();
@endphp

<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Activity name</span>
    <input  class="text-gray-700 p-2 border rounded text-sm @error ('name') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            placeholder="Enter activity name.."
            name="name"
            required
            @if($route==='activities.edit')
            value = "{{ $activity->name }}"
            @else
            value = "{{old('name')}}"
            @endif
    >
</label>

@error('name')
<span class="text-red-500 text-xs">{{$message}}</span>
@enderror



<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Price</span>
    <input  class="text-gray-700 p-2 border rounded text-sm @error ('price') border-red-500 @enderror form-input mt-1 rounded-lg block w-full"
            placeholder="0.00 MVR"
            name="price"
            type="number"
            required
            @if($route==='activities.edit')
            value = "{{ $activity->price }}"
            @else
            value = "{{old('name')}}"
            @endif
    >
</label>

@error('price')
<span class="text-red-500 text-xs">{{$message}}</span>
@enderror


<label class="block p-2">
    <span class="text-gray-700 font-semibold text-sm">Description</span>
    <textarea class="text-gray-700 p-2 border rounded text-sm @error ('description') border-red-500 @enderror  form-textarea rounded-lg mt-1 block w-full"
              rows="3"
              name="description"
              placeholder="Enter some detail or rules"
              required
    >
         @if($route==='activities.edit')
            {{$activity->description}}
        @else
            {{old('description')}}
        @endif

        </textarea>
</label>