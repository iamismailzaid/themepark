<x-nav>
    {{-- navbar link --}}
    @guest
    <x-nav-link route="login">Login</x-nav-link>
    @endguest
    <x-nav-link route="about">About Us</x-nav-link>
    <x-nav-link route="home">Latest Events</x-nav-link>
    <x-nav-link route="themepark.activities">Activities</x-nav-link>
    <x-nav-link route="themepark.hotels">Available Hotels</x-nav-link>
    {{-- <x-nav-link route="home">Acount</x-nav-link> --}}

{{-- dropdown nav --}}
    @auth
        @if(Auth::user()->isAdmin)
        <x-dropdown trigger="ThemePark Management">
            <x-dropdown-link href="{{route('bookings.index')}}">Booking</x-dropdown-link>
            <x-dropdown-link href="{{route('schedules.index')}}">Schedule Events</x-dropdown-link>
            <x-dropdown-link href="{{route('hotels.index')}}">Hotel</x-dropdown-link>
            <x-dropdown-link href="{{route('rooms.index')}}">Room</x-dropdown-link>
            <x-dropdown-link href="{{route('activities.index')}}">Activities</x-dropdown-link>
        </x-dropdown>
        @endif
    @endauth
</x-nav>
