<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @hasSection('title')
            <title>@yield('title') - {{ config('app.name') }}</title>
        @else
            <title>{{ config('app.name') }}</title>
        @endif


        <style>
             [x-cloak] {
			    display: none;
		    }
        </style>

        <link rel="stylesheet" href="/css/app.css">
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
        <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
        @livewireStyles
         @bukStyles

<!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <body class="mx-auto">

        @include('layouts.desktop')

        @yield('body')

        @livewireScripts
        @bukScripts

        <script src="/js/app.js"></script>
    </body>

    <footer class="">
        <div class="flex bg-gray-200 justify-center border p-10">
            <p class="text-sm text-gray-800">
                © Copyright 2020 Themepark Inc. All rights reserved.
            </p>
        </div>
    </footer>
</html>
