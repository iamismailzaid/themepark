@extends('layouts.base')

@section('body')

     @yield('content')
         @if ($message = Session::get('success'))
             <div x-data="{ open : true }" x-show.transition="open" x-on:click="open = false" class="alert-toast  fixed bottom-0 right-0 m-8 w-5/6 md:w-full max-w-sm z-20" >
                 <input type="checkbox" class="hidden" id="footertoast">
                 <label class="close cursor-pointer p-4 flex items-start justify-between w-full bg-green-400 rounded shadow-lg text-sm text-white"
                        title="close"
                        for="footertoast">

                     {{$message}}

                     <svg x-on:click="open = false" class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                         <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
                         </path>
                     </svg>
                 </label>
             </div>
         @endif
@endsection

