<div>
    <label class="block m-2">
        <span class="text-gray-700 font-semibold text-sm">Event</span>
        {{--   select the events --}}
        <select wire:model="eventId" name="event" class="text-gray-700 p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full    @error ('event') border-red-500 @enderror ">
            <option> select the event </option>
            @foreach ($events as $event)
                <option value="{{$event->id}}"> {{$event->name}} </option>
            @endforeach
        </select>

    </label>

    <label class="block m-2">
        <span class="text-gray-700 font-semibold text-sm">Hotel</span>
        {{--   select the hotel --}}
        <select wire:model="hotelId" name="hotel" class="text-gray-700 p-2 border rounded text-sm
            form-input mt-1 rounded-lg block w-full @error ('hotel') border-red-500 @enderror">
            <option> select the hotel </option>
            @foreach ($hotels as $hotel)
                <option value="{{$hotel->id}}"> {{$hotel->name}} </option>
            @endforeach
        </select>

    </label>

    <label class="block m-2">
        <span class="text-gray-700 font-semibold text-sm">Room</span>
        {{--    get the rooms for the given hotel>--}}
        <select name="room"  class="text-gray-700 p-2 border rounded text-sm
                form-input mt-1 rounded-lg block w-full @error ('room') border-red-500 @enderror">
            <option> select the room </option>
            @foreach ($rooms as $room)
                <option value="{{$room->id}}">{{$room->name}} (MVR {{ number_format($room->price,2) }}) </option>
            @endforeach
        </select>
        {{--    </div>--}}
    </label>
</div>
