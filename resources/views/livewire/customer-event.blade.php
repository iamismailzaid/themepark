<div class="flex flex-wrap items-stretch w-full mb-4 relative">
    <div class="flex -mr-px">
        <span class="flex items-center leading-normal bg-gray-400 rounded rounded-r-none border border-r-0 border-grey-light px-3 whitespace-no-wrap text-grey-dark text-sm">
            Event Name
        </span>
    </div>
    <select class="outline-none flex-shrink flex-grow flex-auto leading-normal w-px flex-1 border h-10 border-grey-light
             rounded rounded-l-none px-3 relative focus:border-blue focus:shadow text-gray-600"
            name="event">
        <option value="">select event</option>
        @foreach($events as $event)
            <option value="{{$event->id}}">{{$event->name}}t</option>
        @endforeach

    </select>
</div>