<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'nic' =>'a269873',
            'mobile' =>'7995657',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'isAdmin' => 1,
        ]);


        User::create([
            'name' => 'Customer',
            'nic' =>'A123456',
            'mobile' =>'7995657',
            'email' => 'customer@customer.com',
            'password' => bcrypt('123456'),
            'isAdmin' => 0,
        ]);

    }
}
