<?php

namespace Database\Factories;

use App\Models\Schedule;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Schedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name;
        return [
            'name' => $name,
            'image' => $this->faker->name,
            'slug' => Str::slug($name),
            'starting' => $this->faker->date('d-m-Y'),
            'due' => $this->faker->date('d-m-Y'),
            'description' => $this->faker->paragraph,
        ];
    }
}
