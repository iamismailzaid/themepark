<?php

namespace Database\Factories;

use App\Models\Hotel;
use App\Models\Room;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RoomFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Room::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $hotel_ids =  Hotel::pluck('id');
        $name = $this->faker->name;

        return [
            'name' => $name,
            'image' => 1,
            'slug' => Str::slug($name),
            'hotel_id' => $hotel_ids->random(),
            'type' => $this->faker->domainName,
            'number' => $this->faker->numberBetween(201,101),
            'price' => $this->faker->numberBetween('1000','9999')

        ];
    }
}
